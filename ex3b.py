# This example integrates a system of ODEs with the scipy.integrate module, and plots the resulting trajectories
# 
# We will work with the following system of (nonlinear) ODEs
# dx/dt =  -y - z
# dy/dt = x + ay 
# dz/dt = b + z(x-c)
# where a, b, and c are parameters that must be specified

# 
# Definition of the equations:
# 
from numpy import *
import pylab as p

# Definition of parameters 
a = 0.1
b = 0.1
c = 4.0

def dX_dt(X, t=0):
	""" Return rhs """
	return array([-X[1] - X[2],X[0] + a*X[1],b + X[2]*(X[0]-c)])
#         
# == Integrating the ODE using scipy.integate ==
# 
# Now we will use the scipy.integrate module to integrate the ODEs.
# This module offers a method named odeint, very easy to use to integrate ODEs:
# 
from scipy import integrate

t = linspace(0, 200,  5000)              # time
X0 = array([0.1, 0.5,0.2])                     # initials conditions

X, infodict = integrate.odeint(dX_dt, X0, t, full_output=True)
infodict['message']                     # >>> 'Integration successful.'
# 
# `infodict` is optional, and you can omit the `full_output` argument if you don't want it.
# Type "info(odeint)" if you want more information about odeint inputs and outputs.
# 
# We can now use Matplotlib to plot the evolution of both populations:
# 
x,y,z = X.T

#plot trajectories
fig1=p.figure()
p.hold(True)
p.plot(t, x, 'r-', label='x')
p.plot(t, y, 'b-', label='y')
p.plot(t, z, 'g-', label='z')  
p.grid()
p.legend(loc='best')
p.xlabel('time')
p.ylabel('position')
p.title('Evolution of trajectories')
p.show()

#plot trajectory in phase space
fig2=p.figure()
p.subplot(211)
p.plot(x,y,'b-',label='x vs y')
p.xlabel('x')
p.ylabel('y')
p.title('Trajectory in x-y space')
p.subplot(212)
p.plot(x,z,'r-',label='x vs z')
p.xlabel('x')
p.ylabel('z')
p.title('Trajectory in x-z space')
p.show()

#3d plot of trajectory in phase space
from mpl_toolkits.mplot3d import Axes3D
fig3 = p.figure()
ax = fig3.add_subplot(111, projection='3d')
p.plot(x,y,z)
p.title('Trajectories in full 3d phase space')
p.show()

#***TASK2: loop over c=4,12,18 making a new x-t plot for each case

#-------------------------------------------------------

